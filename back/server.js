var express =require('express');
var userFile =require('./users.json');

var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var totalUsers = 0;


const URL_BASE = '/apitechu/v1/'
const PORT = process.env.PORT || 3000;

//peticion get de todos los users collections
app.get(URL_BASE + 'users',
        function(request,response){
          console.log('GET' + URL_BASE + 'users');
          response.send(userFile);



//          console.log(request.params);
});

//peticion get de todos los users logados
app.get(URL_BASE + 'users/login',
        function(request,response){
          userFileLogin =undefined
          for (i in userFile)
          {
            if(userFile[i].logged ==true )
            {
              userFileLogin = userFile[i];
            }
          }
          if(userFileLogin != undefined){
            response.send(userFileLogin);
          }
          else {
            response.send({"mensaje":"No Existen usuarios logados"})
          }

});


//peticion POST LOGOUT
app.post(URL_BASE + 'logout/:id',
    function(req,res){
    let indice = req.params.id;
    let instancia = userFile[indice-1]
    console.log(instancia);

    if(instancia != undefined)
    {
      if (instancia.logged==true)
      {
        delete instancia.logged;
        writeUserDataToFile(userFile);
        res.status(204);
        res.send({"mensaje":"Usuario hizo logout"})
      }
      else {
        res.send({"mensaje":"Usuario no hizo login"})
      }

    }
    else {
      res.send({"mensaje":"Recurso no encontrado"})
    }
    res.status(204);
});

//peticion POST LOGIN
app.post(URL_BASE + 'login',
    function(req,res){
    var cntBody = Object.keys(req.body).length;

    console.log(cntBody);
    let email = req.body.email;
    let password = req.body.password;
    console.log(email);
    if(cntBody != 0)
    {
      for (i in userFile)
      { console.log(i);
        if(userFile[i].email == email)
        {
          if(userFile[i].password == password)
          {
            userFile[i].logged =true;
            res.status(201);
            res.send({"mensaje": "Usuario logueado : "+email+ " "+ password})
            console.log(userFile);
            writeUserDataToFile(userFile);
          }
          else{
            res.send({"mensaje": "Login Incorrecto "})
          }
        }
      }
    }

    else {
        let varError = {"mensaje": "NO existe parametros en body"}
        res.send(varError);
    }
});


// funcion write
function writeUserDataToFile(data) {
   var fs = require('fs');
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./users.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

//peticion GET de un 'users' instance

app.get (URL_BASE+ 'users/:id',
function(request, response){
  console.log('GET' + URL_BASE + 'user/id');
  let indice = request.params.id;
  let instancia = userFile[indice-1]
  let respuesta =(instancia!=undefined)? instancia:{"mensaje":"Recurso no encontrado"}
  response.status(200);

  response.send(respuesta);
});

// peticion get con query string
app.get (URL_BASE+ 'usersq',
function(request, response){
  console.log('GET' + URL_BASE + ' con query string');
  console.log(request.query);
  response.send(respuesta);
});

// peticion get con query string para limitar el numero de usuarios a mostrar.
app.get (URL_BASE+ 'usersmax',
function(request, response){
  console.log('GET' + URL_BASE + ' con query string max');
  console.log(request.query);
  const queryString = request.query
    if(queryString.indexOf('?') > -1){
      queryString = queryString.split('?')[1];
    }
    var pairs = queryString.split('&');
    var result = {};
    pairs.forEach(function(pair)
    {
      pair = pair.split('=');
      result[pair[0]] = decodeURIComponent(pair[1] || '');
    });
  console.log(result)
});

//peticion POST creacion user
app.post(URL_BASE + 'users',
    function(req,res){
    totalUsers = userFile.length;
    cuerpo=req.body;
    console.log(req.body);
//    if(cuerpo.length = 0)? res.send({"mensaje": "Body vacio")
    let newUser= {
      userID : totalUsers+1,
      first_name : req.body.first_name,
      last_name : req.body.last_name,
      email : req.body.email,
      password : req.body.password
    }
    userFile.push(newUser);
    res.status(201);
    res.send({"mensaje": "Usuario creado con èxito" ,"usuario": newUser})
  }
);

//peticion PUT actualizar
app.put(URL_BASE + 'users/:id',
    function(req,res){
    var cntBody = Object.keys(req.body).length;
    console.log(cntBody);
    if(cntBody != 0)

    {let idx = req.params.id-1;
      console.log(idx);
      if(userFile[idx]!=undefined)
      {
        userFile[idx].first_name = req.body.first_name
        userFile[idx].last_name = req.body.last_name
        userFile[idx].email = req.body.email
        userFile[idx].password= req.body.password
        userFile[idx].first_name = req.body.first_name
        res.send({"mensaje": "Usuario actualizado "+ req.params.id +" "
        + userFile[idx].first_name +" "+ userFile[idx].last_name})
        res.status(203);
      }
      else
      {
        res.send({"mensaje": "Recurso No encontrado"})
      }
    }
    else {
      let varError = {"mensaje": "NO existe parametros en body"}
      res.send(varError);
    }
  }
);

//peticion delete
app.del(URL_BASE + 'users/:id',
    function(req,res){
    let idx = req.params.id-1;
      console.log(idx);
      if(userFile[idx]!=undefined)
      { let first = userFile[idx].first_name
        let last = userFile[idx].last_name
        let email = userFile[idx].email
        let password = userFile[idx].password

        userFile.splice(idx,1)
         res.send({"mensaje": "Usuario borrado "+ req.params.id + " "
        + first + " "+ last + " " + email + " " + password})
        res.status(204);

      }
      else
      {
        res.send({"mensaje": "Recurso No encontrado"})
      }
  }
);

app.listen(PORT,function(){
  console.log('API TechU! eschuchando en puerto sdf...')

});
